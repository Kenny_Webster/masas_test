# -*- coding: cp1252 -*-
import arceditor
import ConfigParser
import arcpy
import imp
import httplib
import datetime, time
import urllib2
import xml.etree.ElementTree as ET
import httplib
from httplib import HTTPConnection, HTTPS_PORT
import ssl, socket

## MASAS Constants

url = "https://sandbox2.masas-sics.ca/hub/feed"
headers = {"Content-Type": "application/atom+xml", "Authorization": "MASAS-Secret ubop42"}
xmlString = """<?xml version="1.0" encoding="UTF-8"?>
<entry xmlns="http://www.w3.org/2005/Atom">
  <category label="Status" scheme="masas:category:status" term="Status1"/>
  <category label="Severity" scheme="masas:category:Severity" term="Severity1"/>
  <category label="Icon" scheme="masas:category:icon" term="Icon1"/>
  <title type="xhtml">
    <div xmlns="http://www.w3.org/1999/xhtml">
      <div xml:lang="en">Title1</div>
    </div>
  </title>
  <content type="xhtml">
    <div xmlns="http://www.w3.org/1999/xhtml">
      <div xml:lang="en">Description1</div>
    </div>
  </content>
  <expires xmlns="http://purl.org/atompub/age/1.0">Expires1</expires>
  <effective xmlns='masas:experimental:time'>Effective1</effective>
  <line xmlns="http://www.georss.org/georss">geo1</line>
</entry>
"""

# Overwrite pre-existing files
arcpy.env.overwriteOutput = True

# Directories...
workingDir = "C:\\Code\\MASAS\\"
scriptsDir = workingDir + "\\Scripts\\"
scratchDir = workingDir + "\\Scratch\\"
scratchGdb = scratchDir + "Scratch.gdb"

# Config Parser
config = ConfigParser.ConfigParser()
config.read(workingDir + "\\Config\\master.ini")

# Load Local Modules
hrmlog = imp.load_source('hrmlog', config.get('LOGGING', 'logModule'))
hrmutils = imp.load_source('hrmutils', config.get('UTILS', 'utilsModule'))

# Logging
logFile = config.get('LOGGING', 'logDir') + str(datetime.date.today()) + "_MASAS.log"
logger = hrmlog.setupLog("hrmlog", logFile)

# Variables
DEVSDE = config.get('SDE_DEVSDE', "sdeFile")
Table = DEVSDE + "\\TRN_street_closure_permit"
f_masasid = 'MASASID'
f_moddate = "MODDATE"
shpFieldName = arcpy.Describe(Table).ShapeFieldName


class HTTPSConnection(HTTPConnection):
    "This class allows communication via SSL."
    default_port = HTTPS_PORT

    def __init__(self, host, port=None, key_file=None, cert_file=None,
            strict=None, timeout=socket._GLOBAL_DEFAULT_TIMEOUT):
        HTTPConnection.__init__(self, host, port, strict, timeout)
        self.key_file = key_file
        self.cert_file = cert_file

    def connect(self):
        "Connect to a host on a given (SSL) port."
        sock = socket.create_connection((self.host, self.port),
                self.timeout)
        if self._tunnel_host:
            self.sock = sock
            self._tunnel()
        # this is the only line we modified from the httplib.py file
        # we added the ssl_version variable
        self.sock = ssl.wrap_socket(sock, self.key_file, self.cert_file, ssl_version=ssl.PROTOCOL_TLSv1)

#now we override the one in httplib
httplib.HTTPSConnection = HTTPSConnection

def parseRow(row):
    geoString =  parseGeo(row.getValue(shpFieldName))
    parsedString = xmlString
    parsedString = parsedString.replace("Status1", "Test")
    parsedString = parsedString.replace("Severity1", "Minor")
    parsedString = parsedString.replace("Icon1", "ems/incident/roadway/roadwayClosure")
    ## Still need to read from specific fields and add data to entry
    parsedString = parsedString.replace("Description1", parseCol(row))
    parsedString = parsedString.replace("geo1", geoString)
    titleString = row.getValue("FULL_NAME") + " From " + row.getValue("FROM_STR") + " To " + row.getValue("TO_STR")
    parsedString = parsedString.replace("Title1", titleString)
    #Two times, Now and 30 mins from now
    newtime = time.gmtime(time.time() + 600)
    now = time.gmtime(time.time())
    #Convert our two times into strings for MASAS
    tempEffTime = time.strftime("%Y-%m-%dT%H:%M:%S", now)
    timestring = time.strftime("%Y-%m-%dT%H:%M:%S", newtime)
    #Replace the assotiated entries
    parsedString = parsedString.replace("Expires1", timestring)
    parsedString = parsedString.replace("Effective1", tempEffTime)

    #This stuff gets the actual value and stores the parsed string in var effectiveTime
    startDate = str(row.getValue("STARTDATE"))
    sStime = time.strptime(startDate, "%Y-%m-%d %H:%M:%S")
    effectiveTime = time.strftime("%Y-%m-%dT%H:%M:%S", sStime)
    
    return parsedString

## Get the geographical points and parse them into a string in the proper format
def parseGeo(feat):
    thisString = ""
    partNum = 0
    for part in feat:
        for pnt in feat.getPart(partNum):
            if pnt:
                thisString += str(pnt.Y) + " " + str(pnt.X) + " "
            else:
                print "Error parsing Geometry"
        partNum += 1
    return thisString[:-1] #remove trailing blank space

## Function to parse all data that doesn't fit into the MASAS format
## This string will appear in the content field
def parseCol(row):
    s = ""
    s += "Date Applied: " + str(row.getValue("DATEAPPL")) + "\r"
    s += "Street Closure Type: " + str(row.getValue("CLOSETYPE")) + "\r"
    s += "Barricade / Cone Requirement: " + str(row.getValue("BARRCONE")) + "\r"
    s += "Reviewed By: " + str(row.getValue("REVIEWBY")) + "\r"
    s += "Date Reviewed: " + str(row.getValue("REVIEWDATE")) + "\r"
    s += "Approved By: " + str(row.getValue("APPROVEBY")) + "\r"
    s += "Date Approved: " + str(row.getValue("APPRVDATE")) + "\r"
    s += "Requested By: " + str(row.getValue("REQUESTBY")) + "\r"
    s += "Remarks: " + str(row.getValue("REMARKS")) + "\r"
    s += "Charge To: " + str(row.getValue("CHARGETO")) + "\r"
    s += "HRM Tender Number: " + str(row.getValue("HRMTNDRNUM")) + "\r"
    s += "Permit Number: " + str(row.getValue("PERMITNUM")) + "\r"
    s += "Street Open in Evening?: " + str(row.getValue("REOPNEVEN")) + "\r"
    s += "Street Open on Weekends?: " + str(row.getValue("REOPNWEEK")) + "\r"
    if (s.find("&") != -1):
        s = s.replace("&", "and") #& symbols are bad, replace them
    return s

## Creates a new MASAS record from a row of data
## Returns a string with the raw masas id returned from their servers
def newMasas(row):
    returnText = "ERROR"
    req = urllib2.Request(url, parseRow(row), headers)
    resp = loadUrl(req)
    rep = resp[0]
    if (resp[1]):
        repString = rep.read()
        xmlRep = ET.fromstring(repString)
        masasID = xmlRep.find('{http://www.w3.org/2005/Atom}id')
        returnText = masasID.text[12:]
    return returnText
        
## Function to load a url from a request, checks for http exceptions and IO errors
## Returns an array with the [0] being the server response (or exception) and [1] is a boolean to indicate whether the request was successful
def loadUrl(request):
    status = False
    try:
        i = urllib2.urlopen(request)
        resp = i
        #print "Code: " + str(i.getcode()) + "\n"
        #print i.info()
        status = True
    except httplib.HTTPException as e:
        resp = e
        status = False
        print str(e) + " ID: " + str(row.getValue("OBJECTID"))
    except IOError as e:
        resp = e
        print str(e) + " ID: " + str(row.getValue("OBJECTID"))
        status = False
    return [resp, status]

## Update cursor using the masas spatial reference
tableRows = arcpy.UpdateCursor(Table, "", \
                              r'GEOGCS["GCS_WGS_1984",' + \
                              'DATUM["D_WGS_1984",' + \
                              'SPHEROID["WGS_1984",6378137,298.257223563]],' + \
                              'PRIMEM["Greenwich",0],' + \
                              'UNIT["Degree",0.017453292519943295]]')

for row in tableRows:
    ## If no MASASID exists in the record, upload a new one to MASAS
    if (row.isNull(f_masasid)):
        sMasas = newMasas(row)
        print "Writing ID ... " + sMasas + " ... to field ... " + f_masasid
        row.setValue(f_masasid, sMasas)
        tableRows.updateRow(row) #write masasID back to table
    ## If there is already a MASASID, parse the data and update the record in MASAS
    else:
        thisID = row.getValue(f_masasid)
        req = urllib2.Request(url + '/' + thisID, parseRow(row), headers)
        req.get_method = lambda: 'PUT'
        rep = loadUrl(req)
        # Update successful
        if rep[1]:
            print "Updating....  " + str(row.getValue(f_masasid))
        # Update failed - so create a new record with MASAS instead
        else:
            sMasas = newMasas(row)
            print "Writing ID ... " + sMasas + " ... to field ... " + f_masasid
            row.setValue(f_masasid, sMasas)
            tableRows.updateRow(row)
        


    
